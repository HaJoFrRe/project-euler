require 'benchmark'
require 'bigdecimal'

def total_of_multiples(limit = 1000, factors = [3,5])
  x = []
  factors.each do |factor|
    i = factor
    while i < limit
      x << i unless x.include?(i)
      i += factor
    end
  end
  total = 0
  x.each do |y|
    total += y
  end
  total
end

def fibonacci_sum(limit = 4000000)
  a = 1
  b = 2
  sum = 2
  while b <= limit
    c = a
    a = b
    b = c + a
    sum += b if b % 2 == 0
  end
  sum
end

def prime_factors(x)
  original_x = x
  possible_factor = 2
  prime_factors = []
  while x > 1
    while x % possible_factor == 0
      prime_factors << possible_factor
      x = x / possible_factor
    end
    possible_factor += 1
  end
  double_check = 1
  prime_factors.each { |factor| double_check *= factor }
  puts "Error!" unless double_check == original_x
  prime_factors.uniq
end

def palindrome_number?(x)
  x.to_s == x.to_s.reverse
end

def find_palindrome(size_of_multipliers = 3)
  maximum = (10 ** size_of_multipliers) - 1
  minimum = 10 ** (size_of_multipliers - 1)
  largest_palindrome = -1
  y = x = maximum
  while x > minimum
    while y > minimum
      if palindrome_number?(x*y)
        largest_palindrome = x*y if x*y > largest_palindrome
      end
      y -= 1
      break if x*y < largest_palindrome
    end
    y = maximum
    x -= 1
    break if x*y < largest_palindrome
  end
  largest_palindrome
end

def divisible_by_range?(x, maximum = 20)
  return false if x < 1
  (1..maximum).each do |y|
    return false if x % y != 0
  end
  true
end

def find_lowest_divisible_by_range(maximum = 20)
  x = 0
  until divisible_by_range?(x, maximum)
    x += maximum
  end
  x
end

def prime_factors_and_exp(x)
  y = x
  z = 2
  temp_factors = {}
  while y != 1
    while y % z == 0
      temp_factors[z] = 0 unless temp_factors.keys.include?(z)
      temp_factors[z] += 1
      y = y / z
    end
    z += 1
  end
  temp_factors
end

def lowest_common_multiple_of_range(maximum = 20)
  max_factors = {}
  (1..maximum).each do |x|
    temp_factors = prime_factors_and_exp(x)
    temp_factors.keys.each do |key|
      max_factors[key] = temp_factors[key] unless max_factors[key] and max_factors[key] > temp_factors[key]
    end
  end
  answer = 1
  max_factors.keys.each do |key|
    answer *= key ** max_factors[key]
  end
  answer
end

Benchmark.bmbm do |x|
  x.report("Problem 1:") { puts total_of_multiples }
  x.report("Problem 2:") { puts fibonacci_sum }
  x.report("Problem 3:") { puts prime_factors(600851475143).sort.last }
  x.report("Problem 4:") { puts find_palindrome }
  # x.report("Problem 5 (naive):") { puts find_lowest_divisible_by_range }
  x.report("Problem 5:") { puts lowest_common_multiple_of_range }
end
