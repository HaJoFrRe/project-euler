require 'benchmark'
require 'bigdecimal'
require_relative 'pr001-005'

def triangle_num_division(number)
  # factors = 2
  #
  # factors += 2 if number % 2 == 0
  #
  # (3..(number/2).ceil-1).each do |x|
  #   factors += 1 if number % x == 0
  # end
  factors = 0
  count = 1
  sqrt_number = Math.sqrt(number).ceil
  until count >= sqrt_number
    if number % count == 0
      factors += 2
    end
    count += 1
  end
  factors += 1 if count == sqrt_number
  factors
end

def first_triangle_number_with_n_factors(num_factors)
  triangle_num = 1
  triangle_index = 1
  current_num_factors = 1
  old_num_factors = 0
  until current_num_factors > num_factors
    triangle_index += 1
    triangle_num += triangle_index
    current_num_factors = triangle_num_division(triangle_num)
    # if (current_num_factors > old_num_factors)
    #   puts "triangle_num = #{triangle_num} -- triangle_index = #{triangle_index} -- current_num_factors = #{current_num_factors}"
    #   old_num_factors = current_num_factors
    # end
  end
  triangle_num
end

Benchmark.bmbm do |x|
  [5,100,500].each do |y|
    x.report("Problem 12 (#{y}):") { puts first_triangle_number_with_n_factors(y) }
  end
end
