require 'benchmark'
require 'bigdecimal'

$thousand_digit_string = """
73167176531330624919225119674426574742355349194934
96983520312774506326239578318016984801869478851843
85861560789112949495459501737958331952853208805511
12540698747158523863050715693290963295227443043557
66896648950445244523161731856403098711121722383113
62229893423380308135336276614282806444486645238749
30358907296290491560440772390713810515859307960866
70172427121883998797908792274921901699720888093776
65727333001053367881220235421809751254540594752243
52584907711670556013604839586446706324415722155397
53697817977846174064955149290862569321978468622482
83972241375657056057490261407972968652414535100474
82166370484403199890008895243450658541227588666881
16427171479924442928230863465674813919123162824586
17866458359124566529476545682848912883142607690042
24219022671055626321111109370544217506941658960408
07198403850962455444362981230987879927244284909188
84580156166097919133875499200524063689912560717606
05886116467109405077541002256983155200055935729725
71636269561882670428252483600823257530420752963450
""".gsub(/\s+/, "")

def square_of_sum(maximum)
  total = 0
  (1..maximum).each do |x|
    total += x
  end
  total ** 2	
end

def sum_of_squares(maximum)
  total = 0
  (1..maximum).each do |x|
    total += x ** 2
  end
  total	
end

def prime_simple?(variant, x)
  y = 2
  upper_bound = Math.sqrt(x).floor if variant == 1  
  while (y < x)
	return true if variant == 1 and y > upper_bound
    return false if x % y == 0
    y += 1
  end
  true
end

def prime?(variant, x, prime_list = [])
  return prime_simple?(variant, x) if variant < 2
  upper_bound = Math.sqrt(x).floor if variant == 3
  prime_list.each do |y|
    return true if variant == 3 and y > upper_bound
    return false if x % y == 0
  end
  true
end

def nth_prime(variant, x)
	case variant
	when 0, 1
      prime_list = []
      y = 2
      while prime_list.size < x
        prime_list << y if prime?(variant, y)
        y += 1
      end
      result = prime_list.last
    when 2, 3
	   prime_list = []
	   y = 2
	   while prime_list.size < x
		  prime_list << y if prime?(variant, y, prime_list)
		  y += 1
	   end
	   result = prime_list.last
	end
	return result
end

def cheat_nth_prime(x)
   $prime_list ||= []
   y = $prime_list.last
   y ||= 1
   y += 1
   while $prime_list.size < x
      $prime_list << y if prime?(y, $prime_list)
      y += 1
   end  
   $prime_list[x - 1]
end

def highest_adjacent_product(num_digits)
	first = 0
	last = first + (num_digits - 1)
	highest = 0
	highest_list = []
	while last < $thousand_digit_string.size
	  product = 1
	  x_list = []
	  (first..last).each do |x|
	    product *= $thousand_digit_string[x].to_i
	    x_list << $thousand_digit_string[x].to_i
	  end
	  highest = product if product > highest  
	  highest_list = x_list if product == highest  
	  # puts "Product of #{highest_list.inspect} is #{highest}" if product == highest
	  first += 1
	  last += 1 
	end
	highest
end

def first_pyth_triplet(desired_sum)
  triplet = [0,0,0]
  (3..desired_sum-3).each do |c|
    (2..c-1).each do |b|
      a = Math.sqrt(c**2 - b**2)
        if a**2 + b**2 == c**2
          if a + b + c == desired_sum
            triplet =  [a, b, c]
            return triplet
          end
        end
    end
  end
  triplet
end

def last_pyth_triplet(desired_sum)
  triplet = [0,0,0]
  (3..desired_sum-3).each do |c|
    (2..c-1).each do |b|
      a = Math.sqrt(c**2 - b**2)
      if a**2 + b**2 == c**2
        if a + b + c == desired_sum
          triplet =  [a, b, c]
        end
      end
    end
  end
  triplet
end

def primes_below(variant, x)
  prime_list = []
  case variant
    when 0, 1
      prime_list = []
      y = 2
      while y < x
        prime_list << y if prime?(variant, y)
        y += 1
      end
      result = prime_list.last
    when 2, 3
      prime_list = []
      y = 2
      while y < x
        prime_list << y if prime?(variant, y, prime_list)
        y += 1
      end
  end
  prime_list
end

Benchmark.bmbm do |x|
  x.report("Problem 6:") { puts square_of_sum(100) - sum_of_squares(100) }
  [10001].each do |y|
	[3].each do |variant|
	  x.report("Problem 7 variant #{variant} (#{y}):") { puts nth_prime(variant, y) }
    end
  end
  [13].each do |y|
	x.report("Problem 8 (#{y}):") { puts highest_adjacent_product(y) }
  end
  y = 1000
  x.report("Problem 9 (#{y}):") { puts first_pyth_triplet(y).reduce(:*) }
  # x.report("Problem 9b (#{y}):") { puts last_pyth_triplet(y).reduce(:*) }
  [2000000].each do |y|
    [3].each do |variant|
      x.report("Problem 10 variant #{variant} (#{y}):") { puts primes_below(variant, y).reduce(:+) }
    end
  end
end
